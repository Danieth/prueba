# Crear archivo propiedades
/src/main/resources/application.yml
```yaml
server:
  address: 0.0.0.0
  port: '8000'
spring:
  datasource:
    driver-class-name: org.h2.Driver
    username: <user db>
    url: jdbc:h2:mem:~/<dbh2>
    platform: h2
    password: <password db>
  h2:
    console:
      enabled: 'true'
      path: /h2
springdoc:
  swagger-ui:
    path: /api/doc
api:
  urlTheMovieDb: https://api.themoviedb.org/3
  token: <token api>
```

# crear jar
```bash
./gradlew assambre
```
# ejecutar java
```bash
java -jar movies-0.0.1-SNAPSHOT.jar
```
# ejectuar docker
```bash
docker-comopse up -d
```
direccion del swagger [swagger](http://localhost:8000/api/swagger-ui/index.html#/) entrar a la base de datos [link](http://localhost:8000/h2/)

