package com.talantaa.movies.utils;

public interface IMapper <I, O>{
    public O map(I in);
}
