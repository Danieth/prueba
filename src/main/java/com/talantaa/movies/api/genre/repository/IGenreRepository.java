package com.talantaa.movies.api.genre.repository;

import com.talantaa.movies.api.genre.models.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IGenreRepository extends JpaRepository<Genre, Long> {
    @Query(value = "SELECT * FROM Genre g WHERE g.ref IN :refs", nativeQuery = true)
    List<Genre> findAllByRef(@Param("refs") Iterable<Long> refs);
}
