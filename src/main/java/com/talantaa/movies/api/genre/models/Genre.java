package com.talantaa.movies.api.genre.models;

import lombok.Data;

import javax.persistence.*;


@Data
@Entity
public class Genre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long ref;
    private String name;
}
