package com.talantaa.movies.api.genre;

import com.talantaa.movies.api.genre.service.GenreService;
import com.talantaa.movies.api.genre.service.dto.GenreOutDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("v1/genre")
public class GenreController {

    @Autowired
    private final GenreService service;

    public GenreController(GenreService service) {
        this.service = service;
    }


    @GetMapping()
    public ResponseEntity<List<GenreOutDTO>> findAll() {
        return ResponseEntity.ok(this.service.findAll());
    }
}
