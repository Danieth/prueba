package com.talantaa.movies.api.genre.service;

import com.talantaa.movies.api.genre.mapper.GenreResponseDtoToGenre;
import com.talantaa.movies.api.genre.mapper.GenreToGenresOutDto;
import com.talantaa.movies.api.genre.models.Genre;
import com.talantaa.movies.api.genre.repository.IGenreRepository;
import com.talantaa.movies.api.genre.service.dto.GenreOutDTO;
import com.talantaa.movies.api.genre.service.dto.GenreResponseInDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GenreService {

    @Value("${api.urlTheMovieDb}")
    private String urlApi;
    @Value("${api.token}")
    private String token;
    @Autowired
    private final IGenreRepository respository;
    @Autowired
    private final GenreResponseDtoToGenre mapper;
    @Autowired
    private final GenreToGenresOutDto mapperOut;

    public GenreService(IGenreRepository respository, GenreResponseDtoToGenre mapper, GenreToGenresOutDto mapperOut) {
        this.respository = respository;
        this.mapper = mapper;
        this.mapperOut = mapperOut;
    }

    private List<Genre> getGenreApi() {
        WebClient.Builder builder = WebClient.builder();
        WebClient.ResponseSpec response = builder.build()
                .get()
                .uri(String.format("%s/genre/movie/list", this.urlApi))
                .header("Authorization", String.format("Bearer  %s", this.token))
                .retrieve();
        GenreResponseInDTO genreJson = response.bodyToMono(GenreResponseInDTO.class).block();
        assert genreJson != null;
        return genreJson.getGenres().stream().map(this.mapper::map).toList();
    }

    public List<GenreOutDTO> findAll() {

        return this.respository.findAll().stream().map(this.mapperOut::map).collect(Collectors.toList());
    }

    public List<Genre> create() {
        return this.respository.saveAll(this.getGenreApi());
    }

    public List<Genre> findAllByRef(Iterable<Long> refs) {
        return this.respository.findAllByRef(refs);
    }

    public List<Genre> findAllById(Iterable<Long> id) {
        return this.respository.findAllById(id);
    }

    public HashMap<Long, Genre> hashMapGenre(Iterable<Long> refs) {
        HashMap<Long, Genre> genres = new HashMap<Long, Genre>();
        for (Genre genre : this.respository.findAllByRef(refs)) genres.put(genre.getRef(), genre);
        return genres;
    }
}
