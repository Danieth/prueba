package com.talantaa.movies.api.genre.mapper;

import com.talantaa.movies.api.genre.models.Genre;
import com.talantaa.movies.api.genre.service.dto.GenreOutDTO;
import com.talantaa.movies.utils.IMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GenreToGenresOutDto implements IMapper<Genre, GenreOutDTO> {
    @Autowired
    private final ModelMapper modelMapper;

    public GenreToGenresOutDto(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public GenreOutDTO map(Genre in) {
        return this.modelMapper.map(in, GenreOutDTO.class);
    }
}
