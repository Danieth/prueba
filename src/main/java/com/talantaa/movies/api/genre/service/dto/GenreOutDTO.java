package com.talantaa.movies.api.genre.service.dto;

import lombok.Data;

@Data
public class GenreOutDTO {
    private Long id;
    private String name;
}
