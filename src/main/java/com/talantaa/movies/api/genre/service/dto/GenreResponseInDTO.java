package com.talantaa.movies.api.genre.service.dto;

import lombok.Data;

import java.util.List;

@Data
public class GenreResponseInDTO {
    private List<GenreJsonInDTO> genres;
}
