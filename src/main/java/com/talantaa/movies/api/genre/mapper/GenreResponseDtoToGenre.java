package com.talantaa.movies.api.genre.mapper;

import com.talantaa.movies.api.genre.models.Genre;
import com.talantaa.movies.api.genre.service.dto.GenreJsonInDTO;
import com.talantaa.movies.utils.IMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GenreResponseDtoToGenre implements IMapper<GenreJsonInDTO, Genre> {
    @Autowired
    private final ModelMapper modelMapper;

    public GenreResponseDtoToGenre(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Genre map(GenreJsonInDTO in) {
        Genre genre = this.modelMapper.map(in, Genre.class);
        genre.setId(null);
        genre.setRef(in.getId());
        return genre;
    }
}
