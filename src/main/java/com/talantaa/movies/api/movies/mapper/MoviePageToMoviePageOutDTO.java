package com.talantaa.movies.api.movies.mapper;

import com.talantaa.movies.api.movies.models.Movie;
import com.talantaa.movies.api.movies.service.dto.internal.MoviePageOutDTO;
import com.talantaa.movies.utils.IMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class MoviePageToMoviePageOutDTO implements IMapper<Page<Movie>, MoviePageOutDTO> {
    @Autowired
    private final ModelMapper modelMapper;
    @Autowired
    private final MovieToMovieOutDto movieToMovieOutDto;

    public MoviePageToMoviePageOutDTO(ModelMapper modelMapper, MovieToMovieOutDto movieToMovieOutDto) {
        this.modelMapper = modelMapper;
        this.movieToMovieOutDto = movieToMovieOutDto;
    }

    @Override
    public MoviePageOutDTO map(Page<Movie> in) {
        MoviePageOutDTO dto = this.modelMapper.map(in, MoviePageOutDTO.class);
        dto.setPages(in.getTotalPages());
        dto.setTotalMovies(in.getTotalElements());
        dto.setMovies(in.getContent().stream().map(this.movieToMovieOutDto::map).collect(Collectors.toList()));
        return dto;
    }
}
