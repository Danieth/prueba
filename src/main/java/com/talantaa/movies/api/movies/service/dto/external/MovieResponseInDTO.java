package com.talantaa.movies.api.movies.service.dto.external;

import lombok.Data;

import java.util.List;

@Data
public class MovieResponseInDTO {
    private Integer page;
    private List<MovieJsonInDTO> results;
}
