package com.talantaa.movies.api.movies.service.dto.internal;

import lombok.Data;

import javax.persistence.Column;
import java.time.LocalDate;
import java.util.List;
@Data
public class MovieOutDTO {
    private Long id;
    private Boolean adult;
    private String backdropPath;
    private String originalLanguage;
    private String originalTitle;
    private String overview;
    private Long popularity;
    private String posterPath;
    private LocalDate releaseDate;
    private String title;
    private Double voteAverage;
    private Long voteCount;
    private Double video;
}
