package com.talantaa.movies.api.movies.config;

import com.talantaa.movies.api.genre.service.GenreService;
import com.talantaa.movies.api.movies.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.stream.IntStream;

@Component
public class StartupApplicationListenerExample implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private final MovieService service;
    @Autowired
    private final GenreService genreService;

    public StartupApplicationListenerExample(MovieService service, GenreService genreService) {
        this.service = service;
        this.genreService = genreService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        this.genreService.create();
        IntStream.range(1, 4).forEachOrdered(this.service::create);
    }
}