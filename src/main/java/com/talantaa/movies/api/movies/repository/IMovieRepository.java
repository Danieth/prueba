package com.talantaa.movies.api.movies.repository;

import com.talantaa.movies.api.movies.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IMovieRepository extends JpaRepository<Movie, Long> {
    @Query(value = "SELECT * FROM Movie m WHERE m.ref IN :refs", nativeQuery = true)
    List<Movie> findAllByRef(@Param("refs") Iterable<Long> refs);
}
