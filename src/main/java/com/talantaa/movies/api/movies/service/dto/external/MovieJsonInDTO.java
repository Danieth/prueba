package com.talantaa.movies.api.movies.service.dto.external;

import com.talantaa.movies.api.genre.service.dto.GenreJsonInDTO;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class MovieJsonInDTO {
    private Boolean adult;
    private String backdrop_path;
    private List<Long> genre_ids;
    private Long id;
    private String original_language;
    private String original_title;
    private  String overview;
    private Double popularity;
    private String poster_path;
    private LocalDate release_date;  //2022-12-14
    private String title;
    private Boolean video;
    private Double vote_average;
    private Long vote_count;
}
