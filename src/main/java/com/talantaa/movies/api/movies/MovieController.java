package com.talantaa.movies.api.movies;

import com.talantaa.movies.api.movies.models.Movie;
import com.talantaa.movies.api.movies.service.MovieService;
import com.talantaa.movies.api.movies.service.dto.internal.MovieDetailOutDTO;
import com.talantaa.movies.api.movies.service.dto.internal.MovieInDTO;
import com.talantaa.movies.api.movies.service.dto.internal.MovieUpdateDTO;
import com.talantaa.movies.api.movies.service.dto.internal.MoviePageOutDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1/movies")
public class MovieController {
    private final MovieService service;

    public MovieController(MovieService movieService) {
        this.service = movieService;
    }

    @GetMapping()
    public ResponseEntity<MoviePageOutDTO> findAll(
            @RequestHeader(value = "page", defaultValue = "0") int page,
            @RequestHeader(value = "size", defaultValue = "20") int size) {
        return ResponseEntity.ok(this.service.findAll(page, size));
    }

    @PostMapping()
    public ResponseEntity<List<Movie>> create(@RequestBody MovieInDTO dto) {
        return ResponseEntity.ok(this.service.create(dto));
    }

    @GetMapping("/{id}")
    public ResponseEntity<MovieDetailOutDTO> detail(@PathVariable("id") Long id) {
        return ResponseEntity.ok(this.service.findById(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable("id") Long id, @RequestBody MovieUpdateDTO dto) {
        this.service.updateById(id, dto);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        this.service.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
