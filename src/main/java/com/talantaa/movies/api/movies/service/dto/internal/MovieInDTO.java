package com.talantaa.movies.api.movies.service.dto.internal;

import lombok.Data;

@Data
public class MovieInDTO {
    private String name;
}
