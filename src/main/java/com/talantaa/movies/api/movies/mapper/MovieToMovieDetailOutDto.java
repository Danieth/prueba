package com.talantaa.movies.api.movies.mapper;

import com.talantaa.movies.api.genre.models.Genre;
import com.talantaa.movies.api.movies.models.Movie;
import com.talantaa.movies.api.movies.service.dto.internal.MovieDetailOutDTO;
import com.talantaa.movies.utils.IMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MovieToMovieDetailOutDto implements IMapper<Movie, MovieDetailOutDTO> {
    @Autowired
    private final ModelMapper modelMapper;

    public MovieToMovieDetailOutDto(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public MovieDetailOutDTO map(Movie in) {
        MovieDetailOutDTO dto = this.modelMapper.map(in, MovieDetailOutDTO.class);
        dto.setGenresName(in.getGenres().stream().map(Genre::getName).toList());
        return dto;
    }
}
