package com.talantaa.movies.api.movies.mapper;

import com.talantaa.movies.api.movies.models.Movie;
import com.talantaa.movies.api.movies.service.dto.internal.MovieOutDTO;
import com.talantaa.movies.utils.IMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MovieToMovieOutDto implements IMapper<Movie, MovieOutDTO> {
    @Autowired
    private final ModelMapper modelMapper;

    public MovieToMovieOutDto(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public MovieOutDTO map(Movie in) {
        MovieOutDTO dto =  this.modelMapper.map(in, MovieOutDTO.class);
//        dto.setGenreName(in.getGenre().getName());
        return dto;
    }
}
