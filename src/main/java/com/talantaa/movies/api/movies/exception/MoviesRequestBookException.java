package com.talantaa.movies.api.movies.exception;

import org.springframework.http.HttpStatus;

public class MoviesRequestBookException extends RuntimeException {

    private HttpStatus httpStatus;

    public MoviesRequestBookException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}