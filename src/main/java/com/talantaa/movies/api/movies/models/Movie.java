package com.talantaa.movies.api.movies.models;

import com.talantaa.movies.api.genre.models.Genre;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long ref;
    private Boolean adult;
    private String backdropPath;
    private String originalLanguage;
    private String originalTitle;
    @Column(columnDefinition="LONGTEXT")
    private String overview;
    private Long popularity;
    private String posterPath;
    private LocalDate releaseDate;
    private String title;
    private Double voteAverage;
    private Long voteCount;
    private Double video;
    @ManyToMany()
    private List<Genre> genres;
}
