package com.talantaa.movies.api.movies.service.dto.external;

import com.talantaa.movies.api.genre.models.Genre;
import lombok.Data;

import java.util.HashMap;

@Data
public class MovieJsonWithGenreDTO {
    private HashMap<Long, Genre> genres;
    private MovieJsonInDTO movieJson;
}
