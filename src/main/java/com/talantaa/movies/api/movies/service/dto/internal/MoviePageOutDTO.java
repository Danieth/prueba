package com.talantaa.movies.api.movies.service.dto.internal;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class MoviePageOutDTO  {
    @JsonProperty("totalPages")
    private int pages;
    @JsonProperty("totalElements")
    private Long totalMovies;
    @JsonProperty("content")
    private List<MovieOutDTO> movies;
}
