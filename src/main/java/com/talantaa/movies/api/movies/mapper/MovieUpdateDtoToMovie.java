package com.talantaa.movies.api.movies.mapper;

import com.talantaa.movies.api.movies.models.Movie;
import com.talantaa.movies.api.movies.service.dto.internal.MovieUpdateDTO;
import com.talantaa.movies.utils.IMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MovieUpdateDtoToMovie implements IMapper<MovieUpdateDTO, Movie> {
    @Autowired
    private  final ModelMapper modelMapper;

    public MovieUpdateDtoToMovie(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Movie map(MovieUpdateDTO in) {
        return this.modelMapper.map(in, Movie.class);
    }
}
