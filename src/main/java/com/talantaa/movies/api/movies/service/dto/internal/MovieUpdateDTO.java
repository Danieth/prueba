package com.talantaa.movies.api.movies.service.dto.internal;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class MovieUpdateDTO {
    private Boolean adult;
    private String backdropPath;
    private String originalLanguage;
    private String originalTitle;
    private String overview;
    private Long popularity;
    private String posterPath;
    private LocalDate releaseDate;
    private String title;
    private Double voteAverage;
    private Long voteCount;
    private Double video;
    @JsonProperty("genresId")
    private List<Long> genreIds;
}
