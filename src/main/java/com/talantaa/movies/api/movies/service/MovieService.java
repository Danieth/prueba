package com.talantaa.movies.api.movies.service;

import com.talantaa.movies.api.genre.models.Genre;
import com.talantaa.movies.api.genre.service.GenreService;
import com.talantaa.movies.api.movies.exception.MoviesRequestBookException;
import com.talantaa.movies.api.movies.mapper.MovieJsonWithGenreDtoToMovie;
import com.talantaa.movies.api.movies.mapper.MoviePageToMoviePageOutDTO;
import com.talantaa.movies.api.movies.mapper.MovieToMovieDetailOutDto;
import com.talantaa.movies.api.movies.mapper.MovieUpdateDtoToMovie;
import com.talantaa.movies.api.movies.models.Movie;
import com.talantaa.movies.api.movies.repository.IMovieRepository;
import com.talantaa.movies.api.movies.service.dto.external.MovieJsonInDTO;
import com.talantaa.movies.api.movies.service.dto.external.MovieJsonWithGenreDTO;
import com.talantaa.movies.api.movies.service.dto.external.MovieResponseInDTO;
import com.talantaa.movies.api.movies.service.dto.internal.MovieDetailOutDTO;
import com.talantaa.movies.api.movies.service.dto.internal.MovieInDTO;
import com.talantaa.movies.api.movies.service.dto.internal.MoviePageOutDTO;
import com.talantaa.movies.api.movies.service.dto.internal.MovieUpdateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
public class MovieService {

    @Value("${api.urlTheMovieDb}")
    private String urlApi;
    @Value("${api.token}")
    private String token;

    @Autowired
    private final IMovieRepository respository;
    @Autowired
    private final MoviePageToMoviePageOutDTO mapperPage;
    @Autowired
    private final MovieJsonWithGenreDtoToMovie mapperResponse;
    @Autowired
    private final GenreService genreService;
    @Autowired
    private final MovieUpdateDtoToMovie mapperIn;
    @Autowired
    private final MovieToMovieDetailOutDto mapperDetail;

    public MovieService(IMovieRepository respository, MoviePageToMoviePageOutDTO mapperPage, MovieJsonWithGenreDtoToMovie mapperResponse, GenreService genreService, MovieUpdateDtoToMovie mapperIn, MovieToMovieDetailOutDto mapperDetail) {
        this.respository = respository;
        this.mapperPage = mapperPage;
        this.mapperResponse = mapperResponse;
        this.genreService = genreService;
        this.mapperIn = mapperIn;
        this.mapperDetail = mapperDetail;
    }


    private List<Movie> getMovieApi(String queryParams, String type) {
        WebClient.Builder builder = WebClient.builder();
        WebClient.ResponseSpec response = builder.build()
                .get()
                .uri(String.format("%s/%s/movie%s", this.urlApi, type, queryParams))
                .header("Authorization", String.format("Bearer  %s", this.token))
                .retrieve();
        MovieResponseInDTO moviesJson = response.bodyToMono(MovieResponseInDTO.class).block();
        assert moviesJson != null;
        ArrayList<Long> genresRef = new ArrayList<>();
        for (MovieJsonInDTO movie : moviesJson.getResults()) genresRef.addAll(movie.getGenre_ids());
        HashMap<Long, Genre> genres = this.genreService.hashMapGenre(genresRef);
        List<Long> moviesRef = this.findAllByRef(moviesJson.getResults().stream().map(MovieJsonInDTO::getId).toList())
                .stream().map(Movie::getRef).toList();
        return moviesJson.getResults().stream().filter(x -> this.validExist(x.getId(), moviesRef)).map(moveJson -> {
            MovieJsonWithGenreDTO movieJsonWithGenreDTO = new MovieJsonWithGenreDTO();
            movieJsonWithGenreDTO.setGenres(genres);
            movieJsonWithGenreDTO.setMovieJson(moveJson);
            return this.mapperResponse.map(movieJsonWithGenreDTO);
        }).toList();
    }

    public List<Movie> findAllByRef(List<Long> refs) {
        return this.respository.findAllByRef(refs);
    }

    private Boolean validExist(Long ref, List<Long> backlist) {
        return !backlist.contains(ref);
    }

    public MoviePageOutDTO findAll(int page, int size) {
        Page<Movie> movies = this.respository.findAll(PageRequest.of(page, size));
        return this.mapperPage.map(movies);
    }

    public MovieDetailOutDTO findById(Long id) {
        Optional<Movie> movie = this.respository.findById(id);
        if (movie.isPresent()) return this.mapperDetail.map(movie.get());
        throw new MoviesRequestBookException("Movie no exists", BAD_REQUEST);
    }

    public void create(int page) {
        this.respository.saveAll(this.getMovieApi(String.format("?page=%d", page), "discover"));
    }

    public List<Movie> create(MovieInDTO dto) {
        return this.respository.saveAll(this.getMovieApi(String.format("?query=%s", dto.getName()), "search"));
    }

    @Transactional
    public void updateById(Long id, MovieUpdateDTO dto) {
        if (this.respository.findById(id).isPresent()) {
            Movie movie = this.mapperIn.map(dto);
            movie.setId(id);
            movie.setGenres(this.genreService.findAllById(dto.getGenreIds()));
            this.respository.saveAndFlush(movie);
        }
    }

    public void deleteById(Long id) {
        this.respository.deleteById(id);
    }
}
