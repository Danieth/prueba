package com.talantaa.movies.api.movies.mapper;

import com.talantaa.movies.api.movies.models.Movie;
import com.talantaa.movies.api.movies.service.dto.external.MovieJsonWithGenreDTO;
import com.talantaa.movies.utils.IMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MovieJsonWithGenreDtoToMovie implements IMapper<MovieJsonWithGenreDTO, Movie> {
    @Autowired
    private final ModelMapper modelMapper;

    public MovieJsonWithGenreDtoToMovie(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Movie map(MovieJsonWithGenreDTO in) {
        Movie movie = this.modelMapper.map(in.getMovieJson(), Movie.class);
        movie.setBackdropPath(in.getMovieJson().getBackdrop_path());
        movie.setOriginalLanguage(in.getMovieJson().getOriginal_language());
        movie.setPosterPath(in.getMovieJson().getPoster_path());
        movie.setReleaseDate(in.getMovieJson().getRelease_date());
        movie.setVoteAverage(in.getMovieJson().getVote_average());
        movie.setVoteCount(in.getMovieJson().getVote_count());
        movie.setRef(in.getMovieJson().getId());
        movie.setGenres(in.getMovieJson().getGenre_ids().stream().map(x -> in.getGenres().get(x)).toList());
        return movie;
    }
}
