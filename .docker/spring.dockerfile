FROM gradle:7.6 as build
RUN mkdir /project
WORKDIR /project
COPY . .
RUN ./gradlew build 
RUN ./gradlew assemble 
RUN java -jar build/libs/movies-0.0.1-SNAPSHOT.jar

FROM eclipse-temurin
RUN mkdir /api
WORKDIR /api
COPY --from=build /project/build/libs/movies-0.0.1-SNAPSHOT.jar /api/app.jar
RUN java -jar app-jar
